# Fiche thématique

## Langage SQL

Vous enseignez la NSI à un groupe de 34 élèves en terminale, vos 6 heures se répartissent
comme suit : 4h en effectif complet (2h + 2h) et 2h en effectif réduit (17 élèves par groupe),
vous avez, pendant ces 2h, une salle de 24 ordinateurs sous GNU/Linux à votre disposition.
Vous désirez préparer une ou plusieurs séances consacrées à la partie “Langage SQL :
requêtes d’interrogation et de mise à jour d’une base de données.” (voir l'extrait du BO ci-dessous)


| Contenu                  | Capacités attendues | Commentaires |
| ------------------------ | ------------------- | --------- |
| Langage SQL : requêtes d'interrogation et de mise à jour d'une base de données | Identifier les composants d'une requête. Construire des requêtes d'interrogation à l'aide des clauses du langage SQL :<br>`SELECT`, `FROM`, `WHERE`, `JOIN`<br><br>Construire des requêtes d'insertion et de mise à jour à l'aide de : `UPDATE`, `INSERT`, `DELETE` | On peut utiliser `DISTINCT`, `ORDER BY` ou les fonctions d'agrégation sas utiliser les clauses `GROUP BY` et `HAVING` |



Après avoir indiqué les prérequis, vous détaillerez les séances que vous désirez mettre en
place (cours magistraux, activités, TD, TP...) avec vos élèves. Vous détaillerez notamment
l'enchaînement de vos séances, le contenu de vos séances (si vous décidez de proposer un
cours “magistral”, vous donnerez un plan détaillé de ce cours), les documents proposés aux
élèves, les logiciels utilisés...