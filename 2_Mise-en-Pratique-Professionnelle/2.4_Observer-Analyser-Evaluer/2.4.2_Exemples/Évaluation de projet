#Évaluer pourquoi ?

La réforme du lycée à vu l’intégration à hauteur de 40 % dans le baccalauréat des notes des bulletins scolaires de première et de terminale pour l’ensemble des disciplines du tronc commun (en dehors du français et de la philosophie) et l’enseignement de spécialité non poursuivi en terminale.
L’évaluation prend donc un rôle certificatif central dans le cursus des élèves au lycée et se doit d’être explicité. Elle doit prendre en compte la progressivité des apprentissages sur les deux années de formation et sur chacune d’elles.
Les critères d’évaluations retenus par les enseignants pour chaque situation d’évaluation seront communiqués aux élèves.


##L’évaluation pour qui ?

Au delà du rôle certificatif de l'évaluation en NSI. Ils est important de comprendre qu'elle ne s'adresse pas qu'aux élèves.


    • Les élèves : les évaluations permettent à l'élève de se situer par rapport au contenu de la discipline et aux compétences qu'elles mobilisents. Elle doivent permettent de proposer des pistes de travail pour progresser.

    • Les enseignants : les évaluations permettent à l'enseignant de réguler son enseignement pour qu'il soit au service des apprentissages des élèves. Elle permettent également de certifier un niveau. L'évaluation peut être vu comme une prise d'information à un instant.

    • Les professeurs : elles permettent de certifier un niveau pour chaque élève

    • Les parents : elles permettent de donner des points de repères
    
    • L’institution : Elles permettent de jalonner un parcours ou encore d'orienter les élèves.

## Différents types d'évaluation

Pour comprendre le rôle central de l'évaluation au service des apprentissages des élèves, il est important de distinguer les différents types d'évaluations pratiqués par les enseignants dans leur quotidien. En effet, elles n'ont pas les mêmes objectifs.

    • L’évaluation diagnostique : elle a pour objet de connaître le niveau de maîtrise des connaissances, des compétences et des capacités des élèves. Pratiquée en début d’année ou en début d’une nouvelle séquence d’apprentissage, elle peut faire l’objet d’une note indicative et/ou d’une appréciation littérale. Elle n'a pas vocation à rentrer dans le calcul des moyennes.
     
    • L’évaluation formative : elle prend sa place en cours d’apprentissage. Elle permet à l’élève de se situer dans l’acquisition des connaissances, des compétences et des capacités grâce aux appréciations régulières portées par l’enseignant, afin de progresser.
      Les évaluations formatives pourront faire l’objet d’une note et/ou d’une appréciation littérale. Les notes des évaluations formatives pourront rentrer dans le calcul de la moyenne.

    • L’évaluation sommative : elle atteste un niveau de maîtrise des connaissances, des compétences et des capacités des élèves et se situe en terme d’un temps d’apprentissage spécifique.

    • L’évaluation certificative : elle est constituée des évaluations formatives et sommatives.
      Les évaluations certificatives donneront lieu à une note comptant pour le calcul de la moyenne.

##Obligation de la note ?

Comme on peut le voir pour les évaluations diagnostiques et formatives, la note n'est pas obligatoire. le retour d'une évaluation peut se faire par une expression littérale et donc sans note.

Ainsi il n'est pas rare de voir des classes sans notes. Toutefois au lycée, et à foritiori en première et terminale, en raison de la réforme et de la part du contrôle continue importante. Ce n'est pas une pratique courante.

Il est toutefois important de garder en tête que l'évaluation ne doit pas se résumer à une note. Celle ci doit être explicitée, justifiée à l'aide de critère clairs et non ambigüs. Ces fameux critères doivent être communiqués aux élèves (et aux familles) pour ne pas que l'évaluation soit décorrélé du reste de l'enseignement.

#Exercice : concevoir la grille d'évaluation d'un projet

Les projets constituent une part important de l'enseignant de NSI. Nous vous proposons ici de créer une grille d'évaluation pour ceux ci que l'on communiquerait en amont aux élèves et d'en discuter sur le forum NSI (Lien à mettre)

On rappelle la partie des programmes de NSI traitant des compétences en informatique et compétences transversales travaillés en informatique : 

Compétences en informatique :
L’enseignement de spécialité de numérique et sciences informatiques permet de développer les compétences suivantes, constitutives de la pensée informatique :
- analyser et modéliser un problème en termes de flux et de traitement d’informations ;
- décomposer un problème en sous-problèmes, reconnaître des situations déjà analysées et réutiliser des solutions ;
- concevoir des solutions algorithmiques ;
- traduire un algorithme dans un langage de programmation, en spécifier les interfaces et les interactions, comprendre et réutiliser des codes sources existants, développer des processus de mise au point et de validation de programmes ;
- mobiliser les concepts et les technologies utiles pour assurer les fonctions d’acquisition, de mémorisation, de traitement et de diffusion des informations ;
- développer des capacités d’abstraction et de généralisation.

Compétences transversales travaillées en informatique :
Cet enseignement se déploie en mettant en activité les élèves, sous des formes variées qui permettent de développer des compétences transversales :
- faire preuve d’autonomie, d’initiative et de créativité ;
- présenter un problème ou sa solution, développer une argumentation dans le cadre d’un débat ;
- coopérer au sein d’une équipe dans le cadre d’un projet ;
- rechercher de l’information, partager des ressources ;
- faire un usage responsable et critique de l’informatique.
