# Une définition de la didactiques

“La didactique est constituée par l'ensemble des procédés, méthodes et techniques qui ont pour but l'enseignement de connaissances déterminées. (...)

A partir du moment où l'on se donne l'objectif légitime de faire accéder aux langages et savoirs fondamentaux l'ensemble d'une population, il convient de **construire des dispositifs qui rendent possible leur appropriation par tous**. Pour cela, il est nécessaire d'étudier très précisément les conditions optimales de cette appropriation et de les reproduire le plus exactement possible dans des "situations didactiques" ; on échappe ainsi - dans l'idéal - à l'aléatoire social et psychologique de l'apprentissage, on crée les conditions pour que, dans la classe, celui-ci se produise "à coup sûr".

Quoique la plupart des didacticiens parlent d'un "triangle didactique" dont les angles sont constitués par "le savoir", "l'apprenant" et "le formateur", en réalité les **trois pôles** de leur réflexion sont, plus précisément, **l'épistémologie de référence de la discipline considérée, la psychologie cognitive et les contraintes de la situation de formation**.

Du côté du pôle épistémologique, on trouve des concepts comme celui de **transposition didactique** (qui désigne les transformations que subit un "savoir savant" pour devenir objet d'enseignement); du côté du pôle psychologique, on trouve des concepts comme ceux de **représentation** (qui désigne la manière dont un apprenant stabilise à un moment donné certaines conceptions) ou d'**opération mentale** (qui désigne la manière de traiter l'information) ; enfin, du côté de la situation de formation, on trouve des concepts comme celui de **contrat didactique** (qui désigne les règles implicites qui régissent la relation enseignant/apprenant). ”

Extraits de la définition de Philippe Meirieu <a href="https://www.meirieu.com/DICTIONNAIRE/didactique.html" target="_blank">https://www.meirieu.com/DICTIONNAIRE/didactique.html</a> par <a href="https://researchportal.unamur.be/fr/persons/julie-henry" target="_blank">Julie Henry</a>.
