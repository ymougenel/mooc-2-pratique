# Entretien avec Olivier Goletti 4/5 : pensée informatique.

Olivier Goletti est un jeune chercheur en didactique de l'informatique. Ces entretiens permettent d'éclairer ce que recouvre 
ce nouveau champs disciplinaire.

## Entretien avec Olivier Goletti, Doctorant en didactique de l'informatique à l'UCLouvain

**Sommaire des 5 vidéos**

* 1/5 [Olivier Goletti, qui es-tu ?](./1_Entretien_Olivier_Goletti1_5.md)
* 2/5 [Didactique de l'informatique ?](./1_Entretien_Olivier_Goletti2_5.md ) 
* 3/5 [Quelques exemples\.](./1_Entretien_Olivier_Goletti3_5.md)
* **4/5 Pensée informatique\.**
* 5/5 [Quelques conseils plus pratiques\.](./1_Entretien_Olivier_Goletti5_5.md)


## 4/5 Pensée informatique

 Le concept de pensée informatique recoupe plusieurs visions en lien avec l'informatique.

[![Entretien Olivier Goletti 4/5](https://mooc-nsi-snt.gitlab.io/portail/assets/Diapo-itw-OG1.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-OG-4.mp4)

Le concept de [pensée informatique ou computationnelle](https://fr.wikipedia.org/wiki/Pens%C3%A9e_computationnelle) recoupe plusieurs visions recoupe plusieurs visions qui ne font pas nécessairement consensus dans la littérature scientifique, après la définition historique de Seymour Papert qui en 1996, définit la pensée computationnelle comme le processus réflexif impliqué dans la formulation de problèmes et de leurs solutions de manière que leur résolution puisse être effectuée par un agent de traitement de l'information. Par exemple la papier de [Jeannette Wing de 2001](https://interstices.info/la-pensee-informatique/) explique avec beaucoup d'élements précis comment utiliser des concepts informatiques dans la résolution de problèmes, par exemple découper un problème en plusieurs petits sous-problèmes, faire de l'évaluation au fil de la résolution, de la généralisation (ou inférence) à partir d'observations concordantes. D'autre part avec les travaux de [Margarida Romero sur le sujet](https://margaridaromero.me/2016/05/05/la-pensee-informatique/), il y a l'ouverture à la démarche créative et l'utilisation de compétences informatiques dans d'autres domaines.

Ces idées sont discutables. D'une part les compétences utilisées dans le domaine de l'informatique ne sont pas nécessairement propres à l'informatique, par exemple la pensée algorithmique (qui est sous-partie de la pensée informatique liée à à une démarche d'organisation, de planification en systématisant et mécanisant un processus de résolution de problème) pré-existait en tant que méthode mathématique. On va aussi chercher dans d'autres disciplines des démarches pré-existantes qui se rattachent à une méthode informatique, comme la généralisation et l'inférence. 
D'autre part, le rêve est celui de transfert de compétences acquises en informatique vers d'autres domaines, mais c'est difficile à montrer, par exemple que la capacité acquise en informatique à découper un problème en sous-problèmes, puisse mener à une capacité générique à mieux pouvoir le faire dans d'autres disciplines (voir quelques exemples précis [ici](https://hal.inria.fr/hal-02281037)), cela reste à prouver dans chaque cas, car les apprentissages restent liés au contexte dans lequel on les fait.




